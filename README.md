
	
1. Set SELinux in permissive mode:
# sed -i 's/SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config
# setenforce 0

2. Install required packages.
# dnf install php -y
# dnf install perl httpd wget unzip glibc automake glibc-common gettext autoconf php php-cli gcc gd gd-devel net-snmp openssl-devel unzip net-snmp postfix net-snmp-utils -y

3. Install Development Tools on RHEL / CentOS 8.
# dnf groupinstall "Development Tools"

4. After the installation, start and enable httpd and php-fpm services.
# systemctl enable --now httpd php-fpm

5. Confirm services status.
# systemctl status httpd php-fpm

6. Change your working directory to /usr/src.
# cd /usr/src

7. Then download and extract Nagios tarball.
# export VER="4.4.5"
# wget https://github.com/NagiosEnterprises/nagioscore/releases/download/nagios-4.4.5/nagios-4.4.5.tar.gz
 # tar -xvf nagios-4.4.1
 
8. Change to created Nagios folder.
# cd nagios-$VER

9. Compiling Nagios Core on RHEL 8 / CentOS 8. After extracting the archive, run the configure script
# ./configure

10. Run the make command with all option to compile the main program and CGIs.
# make all

11. Create User And Group. Below commands creates the nagios user and group. The apache user is also added to the nagios group.
# make install-groups-users
# usermod -a -G nagios apache

12. Install Nagios on RHEL / CentOS 8 base.
# make install

13. Install the init script in /lib/systemd/system.
# make install-daemoninit

14. Install and configures permissions on the directory for holding the external command file.
# make install-commandmode

15.Install sample config files in /usr/local/nagios/etc.
# make install-config

16. Install the Apache config file for the Nagios web interface.
# make install-webconf

17. Installs the Exfoliation theme for the Nagios web interface.
# make install-exfoliation

18. Install the classic theme for the Nagios web interface.
# make install-classicui

19. Create Nagios Web user:
You need to add a user account for accessing the Nagios web interface. 
# sudo htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
Provide password twice.
 
20. Enter and confirm password for the user. You also need to restart the Apache service for changes to take effect:
# systemctl restart httpd

21. Now Install Nagios Plugins:
21.(a) Nagios plugins are used to extend Nagios monitoring features. Switch to /usr/src directory.
# cd /usr/src

21.(b) Download Nagios plugins from Github releases page.
# VER="2.2.1"
#wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
# tar -xvf nagios-pligin

22. Change to the plugins source directory:
# cd nagios-plugins-$VER

23. Compile and install Nagios plugins by running commands below.
# ./configure --with-nagios-user=nagios --with-nagios-group=nagios
# make && make install

24. Confirm that your Nagios installation was successful and working.
# /usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

25. Enable nagios service.
# systemctl  enable --now nagios

26. Service status should indicate running.
# systemctl status nagios
 
27. Access Nagios Web Dashboard. Allow http and https protocols on the firewall.
# firewall-cmd --permanent  --add-service={http,https}
# firewall-cmd --reload


28.	mkdir /usr/local/nagios/etc/servers

chown -R nagios:nagios servers
chmod g+w servers

#29. vim /usr/local/nagios/etc/nagios.cfg
#uncomment line no 51 
#cfg_dir=/usr/local/nagios/etc/servers

#30./usr/local/nagios/etc/objects/command.cfg
#define module as per your requirment

#31. /usr/local/nagios/etc/servers/yourhost.cgf


=====================================================================================================================================
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	Client-Side 	+++++++++++++++++++++++++++++++++++++++++++++++++
=====================================================================================================================================





vi /etc/hosts
server-IP
Cliennt-IP


1.	yum install -y gcc glibc glibc-commom openssl openssl-devel perl wget


2.	Download nrpe packages for remote controol
# cd /usr/src
# wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-4.0.2/nrpe-4.0.2.tar.gz
# tar -xvf nrp-pkg
	./configure
	make all
	
3. Create User And Group. Below commands creates the nagios user and group. The apache user is also added to the nagios group.
# make install-groups-users
# usermod -a -G nagios apache

4. make install

5.	make install-config

6. update services
sh -c  "echo >> /etc/service"

7. sh -c  "sudo echo 'nagios services' >> /etc/services"

8.	sh -c  "sudo echo 'nrpe 5666/tcp' >> /etc/services"

9. make install-init
10. systemctl enable nrpe

11. vim /usr/local/nagios/etc/nrpe.cfg
allowed_ host=127.0.0.1,server-IP
dont_blame nrpe=1

12.	firewall-cmd --permanent  --add-port=5666/tcp

13. firewall-cmd --permanent  --reload

14. systemctl start nrpe
